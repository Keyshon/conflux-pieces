﻿using UnityEngine;
using System.Collections.Generic;
using TileProps;
using GamePlayer;

public class GameManager : MonoBehaviour
{
    // Parameters

    // Tile grid to generate
    public GameObject tileGridPrefab;
    // Hero to generate
    public GameObject heroPrefab;
    // Focused Hero
    public GameObject focusedHero;
    // List of players
    public List<Player> players;
    // Neutral Player
    public Player neutralPlayer;
    // Active player
    public int activePlayerIndex;
    // Skip turn intention
    private bool _endTurnIntent;
    // Emphasis prefabs if highlighted
    private GameObject emph;
    private GameObject emphSelected;
    public Player ActivePlayer
    {
        get
        {
            return players[activePlayerIndex];
        }
    }


    // Functions
    private void Awake()
    {
        emph = Resources.Load("Emphs/EmphPrefab") as GameObject;
        emphSelected = Resources.Load("Emphs/EmphSelectedPrefab") as GameObject;
    }

    void Start()
    {
        _endTurnIntent = false;
        // Neutral Player
        neutralPlayer = new Player("Neutral", 0, new Color(0.3f, 0.3f, 0.3f));
        // Create Players
        players = new List<Player>
        {
            new Player("Player 1", 0, new Color(0.9f, 0.2f, 0.1f)),
            new Player("Player 2", 0, new Color(0.3f, 0.3f, 0.75f))
        };
        // Generate Tile Grid
        tileGridPrefab.GetComponent<TileGrid>().MakeGrid(32, 32);
        // Place heroes
        int i = 0;
        foreach (Player p in players)
        {
            Tile tile = TileGrid.self.tileGrid[i][0].GetComponent<Tile>();
            ClearHard(tile);
            p.SummonHero(tile, Hero.HeroName.Jaime);
            GameObject e = Instantiate(emph, transform);
            GameObject es = Instantiate(emphSelected, transform);
            p.emph = e;
            p.emphSelected = es;
            i++;
        }

        Tile specialTile = TileGrid.self.tileGrid[3][3].GetComponent<Tile>();
        ClearHard(specialTile);
        neutralPlayer.SummonHero(specialTile, Hero.HeroName.Theon);

        activePlayerIndex = 0;
        ActivePlayer.StartTurn();
    }
    
    private void Update() {
        if (_endTurnIntent)
            EndTurn();
    }

    public void EndTurn()
    {
        if (MovingAvailable())
        {
            ActivePlayer.EndTurn();

            activePlayerIndex++;
            if (activePlayerIndex >= players.Count)
                activePlayerIndex = 0;

            ActivePlayer.StartTurn();
            _endTurnIntent = false;
        }
        else
            _endTurnIntent = true;
        if (neutralPlayer.heroes.Count > 0 && neutralPlayer.heroes[0].GetComponent<Hero>().army.Count == 0)
            neutralPlayer.heroes[0].GetComponent<Hero>().Hire(Warrior.WarriorType.Swordsman);
    }

    public void Hire(int warriorNum)
    {
        if (focusedHero != null)
            focusedHero.GetComponent<Hero>().Hire((Warrior.WarriorType)warriorNum);
    }

    public void DismissLast()
    {
        if (focusedHero != null)
            focusedHero.GetComponent<Hero>().Dismiss(focusedHero.GetComponent<Hero>().army.Count - 1);
    }

    public void ClearHard(Tile tile)
    {
        if (tile.taker != null)
            Destroy(tile.taker);
        if (tile.covering == Covering.Types.Mountain)
            tile.SetCovering(Covering.Types.Plain);
        tile.taker = null;
    }

    public bool MovingAvailable()
    {
        foreach (Player p in players)
            foreach (GameObject hero in p.heroes)
                if (!hero.GetComponent<Hero>().isMovingFinished)
                    return false;

        return true;
    }

}
