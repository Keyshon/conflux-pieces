﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Custom/Distinguish"
{
	Properties
	{
		_MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		_Activated("Activated", Float) = 0
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			ZTest[unity_GUIZTestMode]
			Blend SrcAlpha OneMinusSrcAlpha

			Pass
			{
				Name "Default"
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0

				#include "UnityCG.cginc"
				#include "UnityUI.cginc"

				#pragma multi_compile __ UNITY_UI_CLIP_RECT
				#pragma multi_compile __ UNITY_UI_ALPHACLIP

				struct appdata_t
				{
					float4 vertex   : POSITION;
					float4 color    : COLOR;
					float2 texcoord : TEXCOORD0;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct v2f
				{
					float4 pos : SV_POSITION;
					half2 uv : TEXCOORD0;
				};

				sampler2D _MainTex;
				fixed4 _Color;
				fixed4 _TextureSampleAdd;
				float4 _MainTex_ST;
				float _Activated;

				v2f vert(appdata_t v)
				{
					v2f OUT;
					OUT.pos = UnityObjectToClipPos(v.vertex);
					OUT.uv = v.texcoord;
					return OUT;
				}

				fixed4 frag(v2f IN) : COLOR
				{
					half4 color = tex2D(_MainTex, IN.uv);
					half4 outlineC = _Color;
					outlineC.a *= ceil(color.a);
					outlineC.rgb *= outlineC.a;

					//fixed upAlpha = tex2D(_MainTex, IN.uv + fixed2(0, _MainTex_ST.y)).a;
					//fixed downAlpha = tex2D(_MainTex, IN.uv - fixed2(0, _MainTex_ST.y)).a;
					//fixed rightAlpha = tex2D(_MainTex, IN.uv + fixed2(_MainTex_ST.x, 0)).a;
					//fixed leftAlpha = tex2D(_MainTex, IN.uv - fixed2(_MainTex_ST.x, 0)).a;

					if (_Activated > 0)
						if(color.a < 1)
							return outlineC;
					
					
					return color;
				}
			ENDCG
			}
		}
}
