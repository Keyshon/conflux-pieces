﻿using System.Collections.Generic;


namespace TileProps
{
    public class Covering
    {
        // Parameters

        // Covering Type
        public Types Name { get; set; }
        // Modifiers
        public float MovementMod;
        public float GoldMod;
        public float ManaMod;
        public float PopulationMod;
        // Sprite Path
        public string TexturePath;
        // Reachable
        public bool isReachable;
        // Minimal modifier
        public static float MinimalMovementMod 
        { 
            get
            {
                float min = float.MaxValue;

                foreach (Covering c in Type.Values)
                    if (c.MovementMod < min)
                        min = c.MovementMod;
                        
                return min;
            }
        }
        // Tile Covering Names
        public enum Types
        {
            Plain,
            Wood,
            Mountain
        }
        // Available Coverings
        public static Dictionary<Types, Covering> Type = new Dictionary<Types, Covering>
        {
            [Types.Plain] = new Covering
            {
                Name = Types.Plain,
                MovementMod = 1,
                GoldMod = 1,
                ManaMod = 1,
                PopulationMod = 1,
                isReachable = true,
                TexturePath = ""
            },
            [Types.Wood] = new Covering
            {
                Name = Types.Wood,
                MovementMod = 1.5f,
                GoldMod = 0.9f,
                ManaMod = 1.15f,
                PopulationMod = 1.05f,
                isReachable = true,
                TexturePath = "Sprites/StrategicMapSprites/MapSprites/WoodTex"
            },
            [Types.Mountain] = new Covering
            {
                Name = Types.Mountain,
                MovementMod = float.PositiveInfinity,
                GoldMod = 0,
                ManaMod = 0,
                PopulationMod = 0,
                isReachable = false,
                TexturePath = "Sprites/StrategicMapSprites/MapSprites/MountainTex"
            },
        };
    }
}