﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace Helpers
{
    // Rule which resolves duplicate removing error 
    internal class _DuplicateKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable
    {
        #region IComparer<TKey> Members

        public int Compare(TKey x, TKey y)
        {
            int result = x.CompareTo(y);

            if (result == 0)
                return -1;   // Handle equality as beeing lesser
            else
                return result;
        }

        #endregion
    }

    public class SList<T, K> where T : IComparable where K : IComparable
    {
        // Modified sorted list with duplicates
        public SortedList<T, K> list = new SortedList<T, K>(new _DuplicateKeyComparer<T>());

        public void Remove(T obj)
        {
            IList<T> keys = list.Keys;

            for (int i = 0; i < list.Count; i++)
            {
                if (keys[i].CompareTo(obj) == 0)
                {
                    list.RemoveAt(i);
                    return;
                }
            }
        }
    }

    public class Finder
    {
        static public MonoBehaviour FindMonoBehaviour<T>(GameObject gameObject)
        {
            var scripts = gameObject.GetComponents(typeof(MonoBehaviour));

            foreach (MonoBehaviour s in scripts)
            {
                if (s is T)
                    return s;
            }

            return default(MonoBehaviour);
        }

        static public Transform FindDeepChild(Transform parent, string name)
        {
            Queue<Transform> queue = new Queue<Transform>();
            queue.Enqueue(parent);
            while (queue.Count > 0)
            {
                var c = queue.Dequeue();
                if (c.name == name)
                    return c;
                foreach (Transform t in c)
                    queue.Enqueue(t);
            }
            return null;
        }
    }

    public class TextureProcessing
    {
        static public Texture2D Overlay(Texture2D basis, Texture2D addition)
        {
            // Overlay parameters
            Color col;
            float alph;
            // Calculation
            Texture2D result;
            int x = addition.width;
            int y = addition.height;
            result = new Texture2D(x, y);
            // Loop through pixels
            while (x > 0)
            {
                x--;
                y = addition.height;
                while (y > 0)
                {
                    y--;
                    // Use the alpha channel of 2nd image to mix the colors
                    alph = addition.GetPixel(x, y).a;
                    col = addition.GetPixel(x, y);
                    col = col * alph;
                    alph = 1f - alph;
                    col = col + basis.GetPixel(x, y) * alph;
                    result.SetPixel(x, y, col);
                }
            }
            result.Apply();
            return result;
        }
    }
}
