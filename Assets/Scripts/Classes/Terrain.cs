﻿using System.Collections.Generic;


namespace TileProps
{
    public class Terrain
    {
        // Parameters

        // Terrain Type
        public Types Name { get; set; }
        // Modifiers
        public float MovementMod { get; set; }
        public float GoldMod { get; set; }
        public float ManaMod { get; set; }
        public float PopulationMod { get; set; }
        // Sprite Path
        public string TexturePath { get; set; }
        // Tile Terrain Names
        public enum Types
        {
            Grass,
            Badland,
            Desert
        }
        // Available Coverings
        public static Dictionary<Types, Terrain> Type = new Dictionary<Types, Terrain>
        {
            [Types.Grass] = new Terrain
            {
                Name = Types.Grass,
                MovementMod = 1,
                GoldMod = 1,
                ManaMod = 1,
                PopulationMod = 1,
                TexturePath = "Sprites/StrategicMapSprites/MapSprites/GrassSprite"
            },
            [Types.Badland] = new Terrain
            {
                Name = Types.Badland,
                MovementMod = 1,
                GoldMod = 0.9f,
                ManaMod = 0.9f,
                PopulationMod = 1.15f,
                TexturePath = "TerrainTexture/Badland"
            },
            [Types.Desert] = new Terrain
            {
                Name = Types.Desert,
                MovementMod = 0.8f,
                GoldMod = 0.8f,
                ManaMod = 1.4f,
                PopulationMod = 0.9f,
                TexturePath = "TerrainTexture/Desert"
            },
        };
    }
}