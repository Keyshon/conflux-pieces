﻿using System.Collections.Generic;
using UnityEngine;


namespace GamePlayer
{
    public class Player
    {
        public string name;
        public List<GameObject> heroes;
        public int gold;
        public Color color;
        public GameObject emph;
        public GameObject emphSelected;

        public Player(string name, int gold, Color color)
        {
            this.name = name;
            heroes = new List<GameObject>();
            this.gold = gold;
            this.color = color;
        }

        public void SummonHero(Tile tile, Hero.HeroName name)
        {
            heroes.Add(Hero.Spawn(tile, this, name));
        }

        public void EndTurn()
        {
            emph.GetComponent<SpriteRenderer>().enabled = false;
            emphSelected.GetComponent<SpriteRenderer>().enabled = false;

            foreach (GameObject hero in heroes)
            {
                hero.GetComponent<MovableStrategicObject>().RefreshMP();
                hero.GetComponent<HighlightableObject>().Unhighlight();
                hero.GetComponent<MovableStrategicObject>().ClearRoute();
            }
        }

        public void StartTurn()
        {
            emph.GetComponent<SpriteRenderer>().enabled = true;
            emphSelected.GetComponent<SpriteRenderer>().enabled = true;
        }
    }
}