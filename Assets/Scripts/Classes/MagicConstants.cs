﻿/*
 * Constains special constants
 */
using UnityEngine;

public class MagicConstants
{
    /*
     * Sorting orders for painting
     */
    public static int maxObjectsInTile { get {return 4;} }
    public static int weatherOrder { get {return 0;} }
    public static int magicOrder { get {return 0;} }
    public static int takerOrder { get {return 2;} }
    public static int coveringOrder { get {return 3;} }
    public static float baseTileWeight { get {return 20;} }
    public static string spritesPath { get {return Application.dataPath + "/Sprites/"; } }
}
