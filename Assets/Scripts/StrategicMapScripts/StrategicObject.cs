﻿using UnityEngine;
using Helpers;


public class StrategicObject : MonoBehaviour
{
    // Parameters
    public Tile currentTile;

    protected void Start()
    {
        currentTile.taker = gameObject;
    }

    protected void Update()
    {

    }

    public virtual void InteractBy(Leader h)
    {

    }

    // Functions
    protected static GameObject Spawn(string prefabPath, Tile tile)
    {
        GameObject o = Resources.Load(prefabPath) as GameObject;

        StrategicObject strategicObject = Finder.FindMonoBehaviour<StrategicObject>(o) as StrategicObject;

        if (tile.taker != null)
        {
            Debug.Log("Tile is already taken.");
            return null;
        }

        if (strategicObject != null)
        {
            strategicObject.currentTile = tile;
            strategicObject.currentTile.taker = o;
            return o;
        }
        else
        {
            Debug.Log("Object (" + prefabPath + ") has no StrategicObject script attached.");
            return null;
        }
    }
}
