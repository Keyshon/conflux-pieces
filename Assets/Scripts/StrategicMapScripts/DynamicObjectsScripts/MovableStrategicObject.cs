﻿using Helpers;
using System.Collections.Generic;
using UnityEngine;


public class MovableStrategicObject : StrategicObject
{
    // Parameters
    
    // Current movement points
    public float currentMP;
    // Final point cost
    public float finalCost;
    // Maximum of movement points
    public float maxMP;
    // Tile for hero movement
    public Tile destinationTile;
    // Clicked Tiles
    public Tile clickedTile;
    public Tile previousClickedTile;
    // Movement options
    public enum Direction
    {
        U, R, D, L,
        UR, UL, DR, DL,
    };
    // Hero animation speed
    public float objectSpeed = 1.0f;
    public List<Direction> objectRoute;

    // Highlight elements
    public GameObject _greenHighlight;
    public GameObject _endGreenHighlight;
    public GameObject _redHighlight;
    public GameObject _endRedHighlight;
    public bool isMovingFinished;
    // Is player currently moving
    private bool _isMoving;
    // Does player confirm moving
    private bool _movingConfirmed;
    // New tile whcich will be taken
    private GameObject _newTile;
    // Direction of hero movement
    private Direction _direction;
    private bool _recreateRouteIntent;
    // Highlighted route
    private List<GameObject> _highlightedRoute;
    // Is tile changing
    private bool _isChangingTile;

    public override void InteractBy(Leader h)
    {

    }

    public void CreateRoute(Tile destinationTile)
    {
        // Create an intention to create a route
        _recreateRouteIntent = true;
        this.destinationTile = destinationTile;

        if (!_isMoving)
        {
            _movingConfirmed = false;
            _recreateRouteIntent = false;

            // Process the same tile click
            if (destinationTile == null || destinationTile.position.Equals(currentTile.position))
            {
                objectRoute.Clear();
                _highlightedRoute.Clear();
                return;
            }
            _isChangingTile = true;

            ClearRoute();
            
            // Get route        
            objectRoute = currentTile.GetRouteTo(destinationTile, out finalCost);

            _EstimateRoute();
        }
    }

    public void ConfirmMoving()
    {
        _movingConfirmed = true;
    }

    public void ClearRoute()
    {
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("StepPoint"))
            Destroy(go);

        _highlightedRoute.Clear();
    }

    public void RefreshMP()
    {
        currentMP = maxMP;
        ClearRoute();
        CreateRoute(destinationTile);
    }

    public void RelocateTo(GameObject tile)
    {
        // Pick up resource
        if (tile.GetComponent<Tile>().GetTakerType() == Tile.TakerType.Resource)
            tile.GetComponent<Tile>().taker?.GetComponent<Resource>()?.InteractBy(this as Leader);

        // Interact with Strategic Object

        bool clickedInteractable = clickedTile.GetTakerType() == Tile.TakerType.StrategicObject;
        bool finalTile = tile.GetComponent<Tile>().CompareTo(destinationTile) == 0;

        if (clickedInteractable && finalTile)
        {
            if (clickedTile.taker.GetComponent<StrategicObject>() != null)
            {
                if (gameObject.GetComponent<Hero>() != null)
                    clickedTile.taker.GetComponent<StrategicObject>().InteractBy(this as Hero);
                else if (gameObject.GetComponent<Leader>() != null)
                    clickedTile.taker.GetComponent<StrategicObject>().InteractBy(this as Leader);
            }
        }

        currentTile.GetComponent<Tile>().taker = null;
        transform.position = tile.transform.position;
        currentTile = tile.GetComponent<Tile>();
        currentTile.taker = gameObject;
        objectRoute.RemoveAt(0);
    }

    protected void Awake()
    {
        _greenHighlight = Resources.Load<GameObject>("Dots/StepDotAvailable");
        _endGreenHighlight = Resources.Load<GameObject>("Dots/FinalDotAvailable");
        _redHighlight = Resources.Load<GameObject>("Dots/StepDotUnavailable");
        _endRedHighlight = Resources.Load<GameObject>("Dots/FinalDotUnavailable");
    }

    // Start and Update

    protected new void Start()
    {
        base.Start();
        objectRoute = new List<Direction>();
        currentMP = maxMP;
        _movingConfirmed = false;
        _recreateRouteIntent = false;
        isMovingFinished = true;
        _highlightedRoute = new List<GameObject>();
        finalCost = float.MaxValue;
    }

    protected new void Update()
    {
        base.Update();
        if (objectRoute.Count > 0)
        {
            if (_IsEnoughMP())
            {
                if (_movingConfirmed && _isMoving)
                {
                    isMovingFinished = false;
                    if (Vector3.Distance(transform.position, _newTile.transform.position) == 0)
                        _ChangeTile(_newTile);
                    else
                        transform.position = Vector3.MoveTowards(transform.position, _newTile.transform.position, Time.deltaTime * objectSpeed * 1.5f);
                }
                else if (_movingConfirmed && !_isMoving)
                {
                    isMovingFinished = false;
                    _Move(objectRoute[0]);
                }
                else
                    isMovingFinished = true;
            }
            else
                isMovingFinished = true;
        }
        else
        {
            isMovingFinished = true;
            _movingConfirmed = false;
        }

        if (currentTile.position == destinationTile?.position)
            destinationTile = null;

        if (_recreateRouteIntent)
            CreateRoute(destinationTile);
    }

    // Private function and class section

    // Recalculate route
    private void _EstimateRoute()
    {
        Tile beginningTile = currentTile;
        float curMP = currentMP;

        if (objectRoute.Count < 1)
            return;

        for (int i = 0; i < objectRoute.Count; i++)
        {
            beginningTile = beginningTile.GetConnectedTile(objectRoute[i]).GetComponent<Tile>();

            bool enoughPoints = curMP - beginningTile.weight * GetFactor(objectRoute[i]) >= 0;
            
            if (enoughPoints)
            {
                if (beginningTile.position != destinationTile.position)
                    _highlightedRoute.Add(Instantiate(_greenHighlight, beginningTile.position - new Vector2(TileGrid.self.width / 2, TileGrid.self.height / 2), Quaternion.identity));
                else
                    _highlightedRoute.Add(Instantiate(_endGreenHighlight, beginningTile.position - new Vector2(TileGrid.self.width / 2, TileGrid.self.height / 2), Quaternion.identity));

                curMP -= beginningTile.weight * GetFactor(objectRoute[i]);
            }
            else
            {
                if (beginningTile.position != destinationTile.position)
                    _highlightedRoute.Add(Instantiate(_redHighlight, beginningTile.position - new Vector2(TileGrid.self.width / 2, TileGrid.self.height / 2), Quaternion.identity));
                else
                    _highlightedRoute.Add(Instantiate(_endRedHighlight, beginningTile.position - new Vector2(TileGrid.self.width / 2, TileGrid.self.height / 2), Quaternion.identity));

                // If not enogh MP set it to 0 to prevent calculation
                curMP = 0;
            }
        }
    }

    // Release tile status and update them
    private void _ChangeTile(GameObject newTile, bool isCost = true)
    {
        // Release changing tile to allow change it again
        _isChangingTile = false;
        // Other
        _direction = 0;
        _newTile = null;
        _isMoving = false;

        if (isCost)
        {
            // Calculate MP expenditure
            Direction d = 0;

            if (objectRoute.Count > 0)
                d = objectRoute[0];
            else
                return;

            currentMP -= currentTile.GetConnectedTile(d).GetComponent<Tile>().weight * GetFactor(d);


            // Go to the next direction command and change is it available before moving
            RelocateTo(newTile);
        }
        // Delete highlight object when it is reached
        if (!_isChangingTile && _highlightedRoute.Count > 0)
        {
            Destroy(_highlightedRoute[0]);
            _highlightedRoute.RemoveAt(0);
        }

        GetComponent<SpriteRenderer>().sortingOrder =
            -(MagicConstants.maxObjectsInTile * currentTile.position.y + MagicConstants.takerOrder);
    }

    // Move hero in some direction
    private void _Move(Direction direction)
    {
        GameObject newTile = currentTile.GetConnectedTile(direction);
        if (newTile.GetComponent<Tile>().isReachable)
        {
            _newTile = newTile;
            _isMoving = true;
            _direction = direction;
        }
    }

    private bool _IsEnoughMP()
    {
        if (objectRoute.Count > 0)
        {
            Direction d = objectRoute[0];
            return currentMP - currentTile.GetConnectedTile(d).GetComponent<Tile>().weight * GetFactor(d) >= 0;
        }
        else
            return false;
    }

    public static float GetFactor(Direction direction)
    {
        float factor = 1;
        if (direction != Direction.U && direction != Direction.D && direction != Direction.R && direction != Direction.L)
            factor = 1.5f;

        return factor;
    }

    protected new static GameObject Spawn(string prefabPath, Tile tile)
    {
        GameObject o = StrategicObject.Spawn(prefabPath, tile);

        MovableStrategicObject movableStrategicObject = Finder.FindMonoBehaviour<MovableStrategicObject>(o) as MovableStrategicObject;

        if (movableStrategicObject != null)
        {
            movableStrategicObject.maxMP = 120;
            return o;
        }
        else
        {
            Debug.Log("Object (" + prefabPath + ") has no MovableStrategicObject script attached.");
            return null;
        }
    }
}
