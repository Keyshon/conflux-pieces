﻿using GamePlayer;
using Helpers;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Leader
{
    HeroCharacteristics characteristic;
    public string heroName
    {
        get
        {
            return characteristic.name;
        }
    }
    public int heroLevel
    {
        get
        {
            return characteristic.level;
        }
    }

    public enum HeroName
    {
        Theon,
        Jaime
    }

    protected new void Awake()
    {
        base.Awake();
        Heroes = new Dictionary<HeroName, HeroCharacteristics>()
        {
            [HeroName.Theon] = new HeroCharacteristics
            {
                hero = HeroName.Theon,
                sprite = Resources.Load<Sprite>("Sprites/Heroes/Theon"),
                name = "Theon Greyjoy",
                level = 1
            },
            [HeroName.Jaime] = new HeroCharacteristics
            {
                hero = HeroName.Jaime,
                sprite = Resources.Load<Sprite>("Sprites/Heroes/Jaime"),
                name = "Jaime Lannister",
                level = 1
            }
        };
    }
    // Start is called before the first frame update
    protected new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected new void Update()
    {
        base.Update();
    }

    public void InteractBy(Hero h)
    {
        if (h.controllingPlayer.name != controllingPlayer.name)
        {
            Debug.Log("Your hero with power " + h.power + ", fought a hero with power " + power + ".");
            if (power <= h.power)
                Banish();
            else
                h.Banish();
        }
        else
        {
            TwoHeroesUI.Open(h, this);
        }
    }

    public static GameObject Spawn(Tile tile, Player player, HeroName name, int leadership = 100, string prefabPath = "Heroes/HeroPrefab")
    {
        GameObject o = Leader.SpawnEmpty(tile, player, name.ToString(), prefabPath);

        Hero hero = Finder.FindMonoBehaviour<Hero>(o) as Hero;

        if (hero != null)
        {
            GameObject h = Instantiate(o, (Vector2)tile.gameObject.transform.position, Quaternion.identity);
            HeroCharacteristics hc = Heroes[name];
            h.GetComponent<Hero>().characteristic = hc;
            h.GetComponent<Hero>().controllingPlayer = player;
            h.GetComponent<Hero>().leadership = leadership;
            h.GetComponent<SpriteRenderer>().sprite = hc.sprite;

            return h;
        }
        else
        {
            Debug.Log("Object (" + prefabPath + ") has no Leader script attached.");
            return null;
        }
    }

    public class HeroCharacteristics
    {
        public HeroName hero;
        public Sprite sprite;
        public string name;
        public int level;
    }

    protected static Dictionary<HeroName, HeroCharacteristics> Heroes;
}
