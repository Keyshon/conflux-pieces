﻿using UnityEngine;
using GamePlayer;
using Helpers;
using System.Collections.Generic;

public class Leader : MovableStrategicObject
{
    // Parameters

    // Hero name
    public string leaderName;
    public int leadership;
    // Harcode value
    public int power 
    { 
        get 
        {
            int res = 0;
            foreach (WarriorCharacteristics warrior in army)
                res += warrior.power;
            return res;
        } 
    }
    public int takenLeadership
    {
        get
        {
            int res = 0;
            foreach (WarriorCharacteristics warrior in army)
                res += warrior.leadershipCost;
            return res;
        }
    }
    // Controlling Player
    public Player controllingPlayer;
    public List<WarriorCharacteristics> army;

    protected new void Start()
    {
        base.Start();
        army = new List<WarriorCharacteristics>();
    }

    protected new void Update()
    {
        base.Update();
    }

    public void Banish()
    {
        int j = 0;
        for (int i = 0; i < controllingPlayer.heroes.Count; i++)
            if (controllingPlayer.heroes[i].GetComponent<Hero>().heroName == name)
                j = i;
        controllingPlayer.heroes.RemoveAt(j);
        currentTile.taker = null;
        Destroy(gameObject);
    }

    public void Hire(Warrior.WarriorType warrior)
    {
        WarriorCharacteristics w = Warrior.WarriorTypes[warrior];
        if (takenLeadership + w.leadershipCost <= leadership)
        {
            Debug.Log(warrior.ToString() + " has been hired.");
            army.Add(w);
        }
    }

    public void Hire(WarriorCharacteristics wc)
    {
        if (takenLeadership + wc.leadershipCost <= leadership)
        {
            Debug.Log(wc.warriorType.ToString() + " has been hired.");
            army.Add(wc);
        }
    }

    public void Dismiss(int warriorNumber)
    {
        if (warriorNumber >= 0 && warriorNumber < army.Count)
        {
            Debug.Log(army[warriorNumber].warriorType.ToString() + " has been removed.");
            army.RemoveAt(warriorNumber);
        }
    }

    public override void InteractBy(Leader h)
    {
        if (h.controllingPlayer.name != controllingPlayer.name)
        {
            Debug.Log("Your hero with power " + h.power + ", fought a hero with power " + power + ".");
            if (power <= h.power)
                Banish();
            else
                h.Banish();
        }
        else
        {
            TwoHeroesUI.Open(h as Hero, this as Hero);
        }
    }

    // Functions

    public static GameObject Spawn(Tile tile, Player player, string name, string prefabPath = "Leader/LeaderPrefab")
    {
        GameObject o = MovableStrategicObject.Spawn(prefabPath, tile);

        Leader leader = Finder.FindMonoBehaviour<Leader>(o) as Leader;

        if (leader != null)
        {
            GameObject l = Instantiate(o, (Vector2)tile.gameObject.transform.position, Quaternion.identity);
            l.GetComponent<Leader>().controllingPlayer = player;
            l.GetComponent<Leader>().leaderName = name;

            return l;
        }
        else
        {
            Debug.Log("Object (" + prefabPath + ") has no Leader script attached.");
            return null;
        }
    }

    protected static GameObject SpawnEmpty(Tile tile, Player player, string name, string prefabPath)
    {
        GameObject o = MovableStrategicObject.Spawn(prefabPath, tile);

        Leader leader = Finder.FindMonoBehaviour<Leader>(o) as Leader;

        if (leader != null)
        {
            leader.GetComponent<Leader>().controllingPlayer = player;
            leader.GetComponent<Leader>().leaderName = name;

            return o;
        }
        else
        {
            Debug.Log("Object (" + prefabPath + ") has no Leader script attached.");
            return null;
        }
    }
}
