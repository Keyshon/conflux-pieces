﻿using UnityEngine;


public class HighlightableObject : MonoBehaviour
{
    // Material
    public Material mat;
    // Game Manager
    private GameManager _gameManager;

    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        gameObject.GetComponent<SpriteRenderer>().material = Instantiate(mat);
        gameObject.GetComponent<SpriteRenderer>().sharedMaterial.SetColor("_Color", gameObject.GetComponent<Hero>().controllingPlayer.color);
    }

    public void Highlight()
    {
        if (_gameManager.focusedHero != null)
            Unhighlight();
        GetComponent<SpriteRenderer>().sharedMaterial.SetFloat("_Activated", 1.0F);
        _gameManager.focusedHero = gameObject;
    }

    public void Unhighlight()
    {
        _gameManager.focusedHero = null;
        GetComponent<SpriteRenderer>().sharedMaterial.SetFloat("_Activated", .0F);
    }
}
