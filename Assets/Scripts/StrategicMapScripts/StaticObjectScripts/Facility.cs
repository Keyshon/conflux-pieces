﻿using System.Collections.Generic;
using UnityEngine;
using GamePlayer;
using Helpers;

public class Facility : StrategicObject
{
    // Parameters

    // Amount of gold
    public int gold;
    // Treasures
    public List<GameObject> treasures;
    // Guards
    public List<GameObject> guards;
    // Visit status
    public bool isVisited;


    // Functions

    private new void Start()
    {
        base.Start();
        isVisited = false;
        gold = Random.Range(15, 25);
    }

    private new void Update()
    {
        base.Update();
    }

    public override void InteractBy(Leader hero)
    {
        GameObject messageBox = Resources.Load("Messages/FacilityMessagePrefab") as GameObject;
        if (!isVisited)
        {
            Destroy(GameObject.FindGameObjectWithTag("FacilityMessage"));

            foreach (Player player in GameObject.Find("GameManager").GetComponent<GameManager>().players)
                if (player.name == hero.controllingPlayer.name)
                    player.gold += gold;

            messageBox.GetComponent<Message>().Instance(hero.controllingPlayer.name + " earns " + gold + " gold.");
            isVisited = true;
        }
        else
            messageBox.GetComponent<Message>().Instance("It is empty!");
    }

    public static GameObject Spawn(Tile tile, string prefabPath = "Facilities/Tower1")
    {
        GameObject o = StrategicObject.Spawn(prefabPath, tile);

        Facility facility = Finder.FindMonoBehaviour<Facility>(o) as Facility;

        if (facility != null)
        {
            GameObject f = Instantiate(o, (Vector2)tile.gameObject.transform.position + Vector2.up / 2, Quaternion.identity);
            return f;
        }
        else
        {
            Debug.Log("Object (" + prefabPath + ") has no Facility script attached.");
            return null;
        }
    }
}
