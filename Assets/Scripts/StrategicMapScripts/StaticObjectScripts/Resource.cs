﻿using Helpers;
using UnityEngine;

public class Resource : StrategicObject
{
    // Parameters

    // Type of resource
    public enum ResourceType { Gold };
    public int amount;
    public ResourceType resourceType;
    

    // Functions

    protected new void Start()
    {
        base.Start();
        if (resourceType == ResourceType.Gold)
            amount = Random.Range(5, 10);
    }
    
    public new void InteractBy(Leader h)
    {
        h.controllingPlayer.gold += amount;
        Destroy(gameObject);
    }

    public static GameObject Spawn(Tile tile, ResourceType resourceType, string prefabPath = "Res/GoldPrefab")
    {
        GameObject o = StrategicObject.Spawn(prefabPath, tile);

        Resource resource = Finder.FindMonoBehaviour<Resource>(o) as Resource;

        if (resource != null)
        {
            GameObject r = Instantiate(o, (Vector2)tile.gameObject.transform.position, Quaternion.identity);
            r.GetComponent<Resource>().resourceType = resourceType;

            return r;
        }
        else
        {
            Debug.Log("Object (" + prefabPath + ") has no Resource script attached.");
            return null;
        }
    }
}
