﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Helpers;
using TileProps;
using Terrain = TileProps.Terrain;
using Direction = MovableStrategicObject.Direction;

public class Tile : MonoBehaviour, IComparable
{
    // Parameters

    // Type of tile
    public Terrain.Types terrain;
    public Covering.Types covering;
    // Taker
    public GameObject taker;
    // Tile order
    public int order;
    // Tile relative position on grid
    public Vector2Int position
    {
        get
        {
            return new Vector2Int(order % TileGrid.self.width, order / TileGrid.self.width);
        }
    }
    // Can tile be taken by someone
    public bool isReachable
    {
        get
        {
            return Covering.Type[covering].isReachable;
        }
    }
    // Weight in MP
    public float weight
    {
        get
        {
            return MagicConstants.baseTileWeight * Covering.Type[covering].MovementMod;
        }
    }
    public GameManager gameManager
    {
        get
        {
            return GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        }
    }
    // Set of facilities
    public enum TakerType { None, Resource, StrategicObject };
    // Dictionary for dynamic-generated existing textures
    static Dictionary<KeyValuePair<Terrain.Types, Covering.Types>, Texture2D> terrainCoveringMapping = new Dictionary<KeyValuePair<Terrain.Types, Covering.Types>, Texture2D>();

    // Functions

    private void Awake()
    {
        terrainCoveringMapping = new Dictionary<KeyValuePair<Terrain.Types, Covering.Types>, Texture2D>();
    }

    void OnMouseEnter()
    {
        // Generate emphasis
        gameManager.ActivePlayer.emph.transform.position = transform.position;
    }
    
    private void OnMouseOver()
    {
        GameManager gameManager = this.gameManager;

        // Left Mouse Click
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            // Update Emph
            gameManager.ActivePlayer.emphSelected.transform.position = transform.position;

            bool isTaken = taker != null && taker.GetComponent<MovableStrategicObject>() != null;
            bool isRightPlayer = isTaken && taker.GetComponent<Leader>().controllingPlayer.name == gameManager.ActivePlayer.name;

            if (gameManager.focusedHero == null && isRightPlayer)
            {
                taker.GetComponent<HighlightableObject>().Highlight();

                if (taker.GetComponent<MovableStrategicObject>().destinationTile != null)
                    taker.GetComponent<MovableStrategicObject>().CreateRoute(taker.GetComponent<MovableStrategicObject>().destinationTile);
            }
            else if (GameObject.ReferenceEquals(gameManager.focusedHero, taker) && isRightPlayer)
            {
                OneHeroUI.Open(taker.GetComponent<Hero>());
            }
            else
            {
                GameObject hero = gameManager.focusedHero;

                if (hero != null)
                {
                    hero.GetComponent<HighlightableObject>().Unhighlight();
                    hero.GetComponent<MovableStrategicObject>().ClearRoute();
                }
            }
        }

        // Right Mouse Click
        if (Input.GetMouseButtonDown(1))
        {
            // Update Emph
            gameManager.ActivePlayer.emphSelected.transform.position = transform.position;

            if (isReachable)
            {
                GameObject hero = gameManager.focusedHero;
                if (hero != null)
                {
                    MovableStrategicObject mObj = hero.GetComponent<MovableStrategicObject>();
                    
                    mObj.previousClickedTile = mObj.clickedTile;
                    mObj.clickedTile = this;
                    
                    if (mObj.destinationTile == null || !(position == mObj.destinationTile.position))
                    {
                        mObj.CreateRoute(this);

                        bool sameTiles = mObj.clickedTile?.position == mObj.previousClickedTile?.position;
                        bool properPreviousTile = mObj.previousClickedTile?.GetTakerType() == TakerType.StrategicObject;

                        if (sameTiles && properPreviousTile)
                            mObj.ConfirmMoving();
                    }
                    else if (gameManager.MovingAvailable())
                        mObj.ConfirmMoving();

                    if (mObj.objectRoute.Count == 0 && mObj.finalCost < float.MaxValue)
                    {
                        bool tileExist = mObj.previousClickedTile != null && mObj.clickedTile != null;
                        bool clickedFacility = mObj.clickedTile?.GetTakerType() == TakerType.StrategicObject;

                        if (tileExist && clickedFacility && this.CompareTo(mObj.previousClickedTile) == 0)
                        {
                            if (mObj.GetComponent<Hero>() != null)
                                taker.GetComponent<StrategicObject>().InteractBy(mObj as Hero);
                            else if (mObj.GetComponent<Leader>() != null)
                                taker.GetComponent<StrategicObject>().InteractBy(mObj as Leader);
                        }
                    }
                }
            }
        }
    }

    public void SetResource(Resource.ResourceType resourceType)
    {
        GameObject resource = Resource.Spawn(this, resourceType);
        taker = resource;
        resource.GetComponent<SpriteRenderer>().sortingOrder =
            -(MagicConstants.maxObjectsInTile * position.y + MagicConstants.takerOrder);
    }

    public GameObject GetConnectedTile(Direction direction)
    {
        switch (direction)
        {
            case Direction.L:
                return TileGrid.self.GetTile(position + Vector2Int.left);
            case Direction.U:
                return TileGrid.self.GetTile(position + Vector2Int.up);
            case Direction.R:
                return TileGrid.self.GetTile(position + Vector2Int.right);
            case Direction.D:
                return TileGrid.self.GetTile(position + Vector2Int.down);
            case Direction.DL:
                return TileGrid.self.GetTile(position + Vector2Int.down + Vector2Int.left);
            case Direction.UL:
                return TileGrid.self.GetTile(position + Vector2Int.up + Vector2Int.left);
            case Direction.UR:
                return TileGrid.self.GetTile(position + Vector2Int.up + Vector2Int.right);
            case Direction.DR:
                return TileGrid.self.GetTile(position + Vector2Int.down + Vector2Int.right);
            default:
                return null;
        }
    }

    public Dictionary<Direction, GameObject> GetConnectedTiles()
    {
        Dictionary<Direction, GameObject> connectedTiles = new Dictionary<Direction, GameObject>();

        foreach (Direction direction in (Direction[])Enum.GetValues(typeof(Direction)))
            connectedTiles.Add(direction, GetConnectedTile(direction));

        return connectedTiles;
    }

    public void SetCovering(Covering.Types covering)
    {
        // Update Covering
        this.covering = covering;

        // Draw new texture
        Sprite s = Resources.Load<Sprite>(Terrain.Type[terrain].TexturePath);
        if (Covering.Type[covering].TexturePath != "")
        {
            KeyValuePair<Terrain.Types, Covering.Types> pair = new KeyValuePair<Terrain.Types, Covering.Types>(terrain, covering);
            // Add to teture list if new
            if (!terrainCoveringMapping.ContainsKey(pair))
            {
                Sprite coverTex = Resources.Load<Sprite>(Covering.Type[covering].TexturePath);
                Texture2D finalTex = TextureProcessing.Overlay(s.texture, coverTex.texture);
                terrainCoveringMapping[pair] = finalTex;
            }
            Texture2D f = terrainCoveringMapping[pair];
            s = Sprite.Create(f, new Rect(0, 0, f.width, f.height), new Vector2(0.5f, 0.5f));
        }
        gameObject.GetComponent<SpriteRenderer>().sprite = s;
    }

    public void SetFacility()
    {
        GameObject facility = Facility.Spawn(this);
        taker = facility;
        facility.GetComponent<SpriteRenderer>().sortingOrder =
            -(MagicConstants.maxObjectsInTile * position.y + MagicConstants.takerOrder);
    }

    private float _GetPerfectPath(Vector2Int s, Vector2Int f)
    {
        float minMod = Covering.MinimalMovementMod;
        int dx = Math.Abs(s.x - f.x);
        int dy = Math.Abs(s.y - f.y);
        int diagPath = Math.Min(dx, dy);
        int plainPath = Math.Max(dx, dy) - diagPath;
        return (plainPath *  minMod + diagPath * minMod * MovableStrategicObject.GetFactor(Direction.UL)) * MagicConstants.baseTileWeight;
    }
    
    public List<Direction> GetRouteTo(Tile destinationTile, out float finalWeight)
    {

        Vector2Int finalTile = destinationTile.position;
        
        // Set maximum value for all weights
        float[][] weights = new float[TileGrid.self.width][];

        for (int w = 0; w < TileGrid.self.width; w++)
        {
            weights[w] = new float[TileGrid.self.height];

            for (int h = 0; h < TileGrid.self.height; h++)
                weights[w][h] = float.MaxValue;
        }

        // Current tile is 0 weight
        weights[position.x][position.y] = 0;

        List<KeyValuePair<Vector2Int, Vector2Int>> route = new List<KeyValuePair<Vector2Int, Vector2Int>>();

        // Open tiles (distance heuristic)
        SList<float, Tile> open = new SList<float, Tile>();

        open.list.Add(Vector2Int.Distance(position, finalTile), this);

        // Having nodes to check
        while (open.list.Count > 0)
        {
            Tile tile = open.list.Values[0];
            Dictionary<Direction, GameObject> connectedTiles = tile.GetConnectedTiles();

            float k = open.list.Keys[0];

            // Check every connected tile
            foreach (Direction key in connectedTiles.Keys)
            {
                Tile neighbor;

                if (connectedTiles[key] != null)
                    neighbor = connectedTiles[key].GetComponent<Tile>();
                else
                    continue;

                if (neighbor.position != finalTile && (neighbor.GetTakerType() == TakerType.StrategicObject || !neighbor.isReachable))
                    continue;

                // Update weight and index maps
                float newWeight = weights[tile.position.x][tile.position.y] + neighbor.weight * MovableStrategicObject.GetFactor(key);

                // Check new weight and add it to check
                // Is Better Weight ?
                if (newWeight < weights[neighbor.position.x][neighbor.position.y])
                {
                    // Is Better Than Final ?
                    if (newWeight + _GetPerfectPath(neighbor.position, finalTile) < weights[finalTile.x][finalTile.y])
                    {
                        bool isNeighborFree = neighbor.taker == null &&
                                        finalTile != neighbor.position;

                        bool isFinalTile = (finalTile == neighbor.position);

                        // Check new weight and add it to check
                        if ((isNeighborFree || isFinalTile))
                        {
                            weights[neighbor.position.x][neighbor.position.y] = newWeight;
                            
                            // Covering.Type[neighbor.covering].MovementMod modifier is used to avoid using wood tiles at the first place
                            if (finalTile != neighbor.position)
                                open.list.Add(Vector2Int.Distance(neighbor.position, finalTile) * Covering.Type[neighbor.covering].MovementMod, neighbor);

                            route.Add(new KeyValuePair<Vector2Int, Vector2Int>(tile.position, neighbor.position));
                        }
                    }
                }
            }
            open.Remove(k);
        }

        // Restore 
        List<Tile> tileRoute = new List<Tile>();
        List<Direction> finalRoute = new List<Direction>();
        Vector2Int pivot = finalTile;

        finalWeight = weights[finalTile.x][finalTile.y];

        for (int i = route.Count - 1; i >= 0; i--)
        {
            if (route[i].Value == pivot)
            {
                pivot = route[i].Key;
                Vector2Int diff = route[i].Value - pivot;

                if (diff == new Vector2Int(1, 0))
                    finalRoute.Add(Direction.R);
                else if (diff == new Vector2Int(-1, 0))
                    finalRoute.Add(Direction.L);
                else if (diff == new Vector2Int(0, 1))
                    finalRoute.Add(Direction.U);
                else if (diff == new Vector2Int(0, -1))
                    finalRoute.Add(Direction.D);
                else if (diff == new Vector2Int(1, 1))
                    finalRoute.Add(Direction.UR);
                else if (diff == new Vector2Int(1, -1))
                    finalRoute.Add(Direction.DR);
                else if (diff == new Vector2Int(-1, 1))
                    finalRoute.Add(Direction.UL);
                else if (diff == new Vector2Int(-1, -1))
                    finalRoute.Add(Direction.DL);
                else
                    continue;

                tileRoute.Add(TileGrid.self.GetTile(route[i].Value).GetComponent<Tile>());
            }
        }

        finalRoute.Reverse();

        // Check if the last is strategic object 
        bool isStrategicObject = destinationTile.GetTakerType() == TakerType.StrategicObject;
        if (destinationTile.taker != null && isStrategicObject && finalRoute.Count <= 1) 
            return new List<Direction>();
        else if (destinationTile.taker != null && isStrategicObject && finalRoute.Count > 1) 
        {
            // Remove all nodes until diagonal direction
            Direction pd = finalRoute[finalRoute.Count - 1];
            while (finalRoute.Count > 1)
            {
                finalRoute.RemoveAt(finalRoute.Count - 1);
                tileRoute.RemoveAt(0);
                pd = finalRoute[finalRoute.Count - 1];
                if (pd == Direction.DL || pd == Direction.UL || pd == Direction.DR || pd == Direction.UR)
                {
                    finalRoute.RemoveAt(finalRoute.Count - 1);
                    tileRoute.RemoveAt(0);
                    break;
                }
            }
            // Get routes to all 8 neighbors and add the shortest route
            Dictionary<Direction, GameObject> connectedTiles = destinationTile.GetConnectedTiles();
            float minWeight = float.MaxValue;
            List<Direction> addedRoute = new List<Direction>();
            Tile newDestination = null;
            foreach (Direction key in connectedTiles.Keys)
            {
                Tile neighbor;

                if (connectedTiles[key] != null)
                    neighbor = connectedTiles[key].GetComponent<Tile>();
                else
                    continue;

                if (neighbor.GetTakerType() == TakerType.Resource || neighbor.GetTakerType() == TakerType.StrategicObject || !neighbor.isReachable)
                    continue; 
                

                // Check if the shortest route
                List<Direction> addedD = new List<Direction>();
                float w = float.MaxValue;
                if (tileRoute.Count > 0)
                    addedD = tileRoute[0].GetRouteTo(neighbor, out w);
                else
                    addedD = GetRouteTo(neighbor, out w);
                if (w < minWeight)
                {
                    addedRoute = addedD;
                    minWeight = w;
                    newDestination = neighbor;
                }
            }
            finalRoute.AddRange(addedRoute);
            taker.GetComponent<MovableStrategicObject>().destinationTile = newDestination;
        }
        return finalRoute;
    }

    public bool IsConnectedTo(Tile destinationTile)
    {
        return GetRouteTo(destinationTile, out float t).Count > 0;
    }

    public TakerType GetTakerType()
    {
        if (taker == null)
            return TakerType.None;
        else if (taker.GetComponent<Resource>() != null)
            return TakerType.Resource;
        else
            return TakerType.StrategicObject;
    }
    
    public int CompareTo(object obj)
    {
        if (obj == null) return 1;
        
        Tile otherTile = obj as Tile;

        if (otherTile != null)
        {
            if (position.Equals(otherTile.position))
                return 0;
            else
                return 1;
        }
        else
            throw new ArgumentException("Object is not a Tile");
    }
}
