﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamePlayer;

public class Emph : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
