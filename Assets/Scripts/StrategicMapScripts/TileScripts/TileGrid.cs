﻿using UnityEngine;
using TileProps;

public class TileGrid : MonoBehaviour
{
    // Parameters

    public static TileGrid self = null;
    // Set of tile prefabs
    public GameObject plainTile;
    // Game resources
    private GameObject goldResourcePrefab;
    // Grid of all tiles in the game
    public GameObject[][] tileGrid;
    // Accessors
    public int width { get; private set; }
    public int height { get; private set; }
    // Tile Grid positions
    public float minX { get; private set; }
    public float minY { get; private set; }
    public float maxX { get; private set; }
    public float maxY { get; private set; }

    // Functions 

    private void Awake()
    {
        goldResourcePrefab = Resources.Load("Res/GoldPrefab") as GameObject;
        if (self == null)
            self = this;
    }

    public void MakeGrid(int width = 64, int height = 64)
    {
        tileGrid = new GameObject[width][];
        for (int x = 0; x < width; x++)
        {
            tileGrid[x] = new GameObject[height];
            for (int y = 0; y < height; y++)
            {
                tileGrid[x][y] = Instantiate(plainTile, new Vector2(x - width / 2, y - height / 2), Quaternion.identity);
                tileGrid[x][y].transform.parent = gameObject.transform;
                tileGrid[x][y].GetComponent<Tile>().order = width * y + x;
            }
        }

        this.width = width;
        this.height = height;

        // Adjust tile types
        for (int x = 0; x < this.width; x++)
        {
            for (int y = 0; y < this.height; y++)
            {
                int randomTileType = Random.Range(0, 100);
                Tile currentTile = tileGrid[x][y].GetComponent<Tile>();

                // Random Covering Generator
                if (randomTileType < 15)
                {
                    Tile leftTile, rightTile, bottomTile, topTile;

                    bool horizontalRouteAvailable = false;
                    bool verticalRouteAvailable = false;

                    if (x > 0 && x < this.width - 1)
                    {
                        leftTile = tileGrid[x - 1][y].GetComponent<Tile>();
                        rightTile = tileGrid[x + 1][y].GetComponent<Tile>();

                        horizontalRouteAvailable = leftTile.isReachable && leftTile.IsConnectedTo(rightTile);
                    }
                    else if (x == this.width - 1 && y > 0 && y < this.height - 1)
                    {
                        leftTile = tileGrid[x - 1][y].GetComponent<Tile>();
                        bottomTile = tileGrid[x][y - 1].GetComponent<Tile>();
                        topTile = tileGrid[x][y + 1].GetComponent<Tile>();

                        verticalRouteAvailable = !leftTile.isReachable && bottomTile.IsConnectedTo(topTile);
                    }

                    // Do not block two sections with mountains
                    if (x == 0 || verticalRouteAvailable || horizontalRouteAvailable)
                        currentTile.SetCovering(Covering.Types.Mountain);
                    else
                        currentTile.SetCovering(Covering.Types.Plain);
                }
                else if (randomTileType < 30)
                    currentTile.SetCovering(Covering.Types.Wood);
                else
                    currentTile.SetCovering(Covering.Types.Plain);

                // Generate Facilities
                if (currentTile.covering != Covering.Types.Mountain)
                {
                    int facilityType = Random.Range(0, 100);
                    if (facilityType < 10)
                        currentTile.SetFacility();
                    else
                    {
                        int resourceType = Random.Range(0, 100);
                        if (resourceType < 10)
                            currentTile.SetResource(Resource.ResourceType.Gold);
                    }
                }
            }
        }
    }

    public GameObject GetTile(Vector2Int position)
    {
        if (position.x >= 0 && position.x < width && position.y >= 0 && position.y < height)
            return tileGrid[position.x][position.y];
        else
            return null;
    }

    private void Start() 
    {
        minX = tileGrid[0][0].transform.position.x;
        minY = tileGrid[0][0].transform.position.y;
        maxX = tileGrid[width - 1][height - 1].transform.position.x;
        maxY = tileGrid[width - 1][height - 1].transform.position.y;
    }
}
