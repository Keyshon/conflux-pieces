﻿using Helpers;
using UnityEngine;
using UnityEngine.UI;

public class OneHeroUI : PopupUI
{
    Hero hero;
    public static string prefabPath
    {
        get
        {
            return "Messages/OneHeroUI";
        }
    }

    protected new void Start()
    {
        base.Start();
    }

    protected new void Update()
    {
        base.Update();
    }

    public static GameObject Open(Hero hero)
    {
        GameObject prefab = Resources.Load<GameObject>(prefabPath);
        GameObject oneHeroUIObject = Instantiate(prefab, new Vector3(426.5f, 240, 0), Quaternion.identity);
        OneHeroUI oneHero = oneHeroUIObject.GetComponent<OneHeroUI>();

        oneHero.hero = hero;

        string heroTitle = hero.heroName + ", Level " + hero.heroLevel;
        Finder.FindDeepChild(oneHeroUIObject.transform, "HeroNameText").GetComponent<Text>().text = heroTitle;
        string heroMP = "Movement Points: " + hero.currentMP + "/" + hero.maxMP;
        Finder.FindDeepChild(oneHeroUIObject.transform, "HeroMPText").GetComponent<Text>().text = heroMP;

        oneHero.Reload();

        return oneHeroUIObject;
    }

    public override void Reload()
    {
        Transform army = Finder.FindDeepChild(transform, "HeroArmyCanvas");

        foreach (Transform child in army)
            Destroy(child.gameObject);

        for (int i = 0; i < hero.army.Count; i++)
        {
            GameObject li = WarriorListItemUI.addWarriorUI(army, false);
            li.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -li.GetComponent<RectTransform>().rect.height * (i + 1));
            li.GetComponent<WarriorListItemUI>().SetWarrior(hero.army[i], i, hero);
        }
    }
}
