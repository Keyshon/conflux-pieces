﻿using UnityEngine;
using UnityEngine.UI;

public class OnMoveButtonClick : MonoBehaviour
{
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(Click);
    }

    public void Click()
    {
        TwoHeroesUI block = GetComponentInParent<TwoHeroesUI>();
        WarriorListItemUI item = GetComponentInParent<WarriorListItemUI>();

        if (item.hero.heroName != block.rightHero.heroName)
            GetComponentInParent<WarriorListItemUI>().MoveWarrior(block.rightHero);
        else
            GetComponentInParent<WarriorListItemUI>().MoveWarrior(block.leftHero);
    }
}
