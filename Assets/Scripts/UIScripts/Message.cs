﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Message : MonoBehaviour
{
    public GameObject Instance(string text)
    {
        GameObject res = Instantiate(this.gameObject, new Vector3(426.5f, 240, 0), Quaternion.identity);
        GameObject.Find("FacilityMessageText").GetComponent<Text>().text = text;
        return res;
    }

    public void SelfDestroy()
    {
        Destroy(gameObject);
    }
}
