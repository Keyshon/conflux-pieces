﻿using UnityEngine;
using UnityEngine.UI;

public class OnDismissButtonClick : MonoBehaviour
{
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(Click);
    }

    public void Click()
    {
        GetComponentInParent<WarriorListItemUI>().RemoveWarrior();
    }
}
