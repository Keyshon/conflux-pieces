﻿using UnityEngine;
using UnityEngine.UI;

public class OnCloseButtonClick : MonoBehaviour
{
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(Click);
    }

    public void Click()
    {
        gameObject.GetComponentInParent<PopupUI>().Close();
    }
}
