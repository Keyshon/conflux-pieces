﻿using UnityEngine.UI;
using UnityEngine;
using Helpers;

public class TwoHeroesUI : PopupUI
{
    public Hero leftHero;
    public Hero rightHero;
    public static string prefabPath
    {
        get
        {
            return "Messages/TwoHeroesUI";
        }
    }

    protected new void Start()
    {
        base.Start();
    }

    protected new void Update()
    {
        base.Update();
    }

    public static GameObject Open(Hero leftHero, Hero rightHero)
    {
        GameObject prefab = Resources.Load<GameObject>(prefabPath);
        GameObject TwoHeroesUIObject = Instantiate(prefab, new Vector3(426.5f, 240, 0), Quaternion.identity);
        TwoHeroesUI twoHeroes = TwoHeroesUIObject.GetComponent<TwoHeroesUI>();

        twoHeroes.leftHero = leftHero;
        twoHeroes.rightHero = rightHero;
        Finder.FindDeepChild(TwoHeroesUIObject.transform, "LeftHeroNameText").GetComponent<Text>().text = leftHero.heroName;
        Finder.FindDeepChild(TwoHeroesUIObject.transform, "RightHeroNameText").GetComponent<Text>().text = rightHero.heroName;

        twoHeroes.Reload();

        return TwoHeroesUIObject;
    }

    public override void Reload()
    {
        Transform leftCanvas = Finder.FindDeepChild(transform, "LeftHeroArmyCanvas");
        Transform rightCanvas = Finder.FindDeepChild(transform, "RightHeroArmyCanvas");

        foreach (Transform child in leftCanvas)
            Destroy(child.gameObject);

        foreach (Transform child in rightCanvas)
            Destroy(child.gameObject);

        for (int i = 0; i < leftHero.army.Count; i++)
        {
            GameObject li = WarriorListItemUI.addWarriorUI(leftCanvas);
            li.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -li.GetComponent<RectTransform>().rect.height * (i + 1));
            li.GetComponent<WarriorListItemUI>().SetWarrior(leftHero.army[i], i, leftHero);
        }

        for (int i = 0; i < rightHero.army.Count; i++)
        {
            GameObject li = WarriorListItemUI.addWarriorUI(rightCanvas);
            li.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -li.GetComponent<RectTransform>().rect.height * (i + 1));
            li.GetComponent<WarriorListItemUI>().SetWarrior(rightHero.army[i], i, rightHero);
            Finder.FindDeepChild(li.transform, "TransferButtonText").GetComponent<Text>().text = "<";
        }
    }
}
