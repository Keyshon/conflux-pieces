﻿using UnityEngine;
using Helpers;
using UnityEngine.UI;

public class WarriorListItemUI : MonoBehaviour
{
    public WarriorCharacteristics wc;
    public int order;
    public Hero hero;
    public static string prefabPath
    {
        get
        {
            return "Messages/Components/WarriorListItemUI";
        }
    }

    public static GameObject addWarriorUI(Transform canvas, bool twoHeroes = true)
    {
        GameObject warUIPrefab = Resources.Load<GameObject>(prefabPath);
        GameObject li = Instantiate(warUIPrefab, canvas);
        // One hero option - no transfer button. Better to create prefab variant.
        if (!twoHeroes)
        {
            Transform transferBtn = Finder.FindDeepChild(li.transform, "TransferButton");
            Destroy(transferBtn.gameObject);
        }
        return li;
    }

    public void SetWarrior(WarriorCharacteristics wc, int order, Hero hero)
    {
        this.wc = wc;
        this.order = order;
        this.hero = hero;
        Finder.FindDeepChild(gameObject.transform, "WarriorName").GetComponent<Text>().text = wc.warriorType.ToString();
    }

    public void RemoveWarrior()
    {
        hero.Dismiss(order);
        GetComponentInParent<PopupUI>().Reload();
    }

    public void MoveWarrior(Hero newLeader)
    {
        hero.Dismiss(order);
        newLeader.Hire(wc);
        GetComponentInParent<PopupUI>().Reload();
    }
}
