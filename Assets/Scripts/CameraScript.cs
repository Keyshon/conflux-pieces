﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    // Parameters

    // Camera Speed
    public float cameraSpeed = 0.6f;
    public float outbound = 0.5f;
    // Borders when pointer moves screen
    public Vector2 screenBorders = new Vector2(32, 32);

    float _cornerFactor = 1.4f;

    Vector3 _maxStageDimensions;
    Vector3 _minStageDimensions;

    float _screenMaxX; // max x of visible area
    float _screenMaxY; // max y of visible area
    float _screenMinX; // min ...
    float _screenMinY; // min ...
    float _sizeFactor;

    void Start()
    {
        _maxStageDimensions = Camera.main.ScreenToWorldPoint(
            new Vector3(Screen.width, Screen.height, 0));
        _minStageDimensions = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        _screenMaxX = _maxStageDimensions.x;
        _screenMaxY = _maxStageDimensions.y;
        _screenMinX = _minStageDimensions.x;
        _screenMinY = _minStageDimensions.y;
        _sizeFactor = Camera.main.orthographicSize;
    }

    void Update()
    {
        float frameSpeed = cameraSpeed * _sizeFactor * Time.deltaTime;

        // New position of player
        Vector3 pos = transform.position;

        bool moveUp = 
            (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - screenBorders.y)
            && _screenMaxY < TileGrid.self.maxY + outbound;

        bool moveDown =
            (Input.GetKey("s") || Input.mousePosition.y < screenBorders.y)
            && _screenMinY > TileGrid.self.minY - outbound;

        bool moveRight =
            (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - screenBorders.x)
            && _screenMaxX < TileGrid.self.maxX + outbound;

        bool moveLeft =
            (Input.GetKey("a") || Input.mousePosition.x < screenBorders.x)
            && _screenMinX > TileGrid.self.minX - outbound;

        int vDirection = 0; // vertical direction
        if (moveUp)
            vDirection = 1;
        else if (moveDown)
            vDirection = -1;

        int hDirection = 0; // horizontal direction
        if (moveRight)
            hDirection = 1;
        else if (moveLeft)
            hDirection = -1;

        float dx = 0;
        float dy = 0;

        // if moving is diagonal, decrease coodinats speed
        if (Mathf.Abs(vDirection) == 1 && Mathf.Abs(hDirection) == 1)
        {
            dx = hDirection * frameSpeed / _cornerFactor;
            dy = vDirection * frameSpeed / _cornerFactor;
        }
        else
        {
            dx = hDirection * frameSpeed;
            dy = vDirection * frameSpeed;
        }

        pos.x += dx;
        pos.y += dy;

        // idea:
        // the max and min coordinats of visible area
        // move like new position
        _screenMaxX += dx;
        _screenMinX += dx;

        _screenMaxY += dy;
        _screenMinY += dy;

        transform.position = pos;
    }
}
