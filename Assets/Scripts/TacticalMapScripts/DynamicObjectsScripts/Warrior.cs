﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : TacticalObject
{
    public enum WarriorType
    {
        Peasant,
        Swordsman,
        Archer,
    }
    public WarriorCharacteristics characteristics;

    protected new void Start()
    {
        base.Start();
    }

    protected new void Update()
    {
        base.Update();
    }

    public new static GameObject Spawn()
    {
        GameObject o = TacticalObject.Spawn();
        return o;
    }

    public static Dictionary<WarriorType, WarriorCharacteristics> WarriorTypes = new Dictionary<WarriorType, WarriorCharacteristics>()
    {
        [WarriorType.Peasant] = new WarriorCharacteristics {
            warriorType = WarriorType.Peasant,
            texturePath = "Warriors/Peasant",
            leadershipCost = 10,
            goldCost = 2,
            power = 2
        },
        [WarriorType.Swordsman] = new WarriorCharacteristics {
            warriorType = WarriorType.Swordsman,
            texturePath = "Warriors/Swordsman",
            leadershipCost = 10,
            goldCost = 4,
            power = 3
        },
        [WarriorType.Archer] = new WarriorCharacteristics {
            warriorType = WarriorType.Archer,
            texturePath = "Warriors/Archer",
            leadershipCost = 12,
            goldCost = 4,
            power = 2
        },
    };
}

public class WarriorCharacteristics 
{
    public Warrior.WarriorType warriorType { get; set; }
    public string texturePath { get; set; }
    public int leadershipCost { get; set; }
    public int goldCost { get; set; }
    public int power { get; set; } // Hardcode value
}